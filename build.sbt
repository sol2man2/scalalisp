name := "Scala Lisp"
 
version := "0.1"
 
scalaVersion := "2.9.1"

libraryDependencies ++= Seq(
//	"com.novocode" %% "junit-interface" % "0.7" % "test->default",
//	"junit" %% "junit" % "4.8" % "test",
	"org.scalatest" %% "scalatest" % "1.6.1" % "test",
	"org.specs2" %% "specs2" % "1.6.1",
	"org.specs2" %% "specs2-scalaz-core" % "6.0.1" % "test"
)

resolvers ++= Seq(
//	"junit interface repo" at "https://repository.jboss.org/nexus/content/repositories/scala-tools-releases",
	"snapshots" at "http://scala-tools.org/repo-snapshots",
	"releases"  at "http://scala-tools.org/repo-releases"
)
