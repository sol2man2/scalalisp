package com.lge.swcdc.scalalisp

import scala.util.matching.Regex
import java.util.AbstractList
import scala.collection.mutable.HashMap
import scala.collection.immutable.ListMap

case class AbstractLisp 
case class LispAtom(atom: String) extends AbstractLisp 
case class LispList(atoms: List[Any])  extends AbstractLisp
case class LispLeftParenthesis(leftParenthesis: String) extends AbstractLisp
case class LispRightParenthesis(rightParenthesis: String) extends AbstractLisp
case class LispOperator(operator: String) extends AbstractLisp
case class LispMacro(macro: String) extends AbstractLisp

class Lisp {

	val LEFT_PARENTHESYS = "("
	val RIGHT_PARENTHESYS = ")"
	val QUOTE = "QUOTE"
	val CAR = "CAR"
	val CDR = "CDR"
	val CONS = "CONS"
	val EQUAL = "EQUAL"
	val ATOM = "ATOM"
	val COND = "COND"
	val LAMBDA = "LAMBDA"
	val LABEL = "LABEL"

	val regexLeftParenthesis = new Regex("""(?i)(QUOTE)|(CAR)|(CDR)|(CONS)|(EQUAL)|(ATOM)|(COND)|(LAMBDA)|(LABEL)|\(|\)|([\w\d\+\.\-\*\!/]+)""")
	def getLispToken(str: String): List[AbstractLisp] = {

		val trimmed = str.trim
		regexLeftParenthesis.findFirstIn(trimmed) match {
			case Some(lParenthesis) if lParenthesis.toUpperCase().equals(LEFT_PARENTHESYS) =>
				LispLeftParenthesis(lParenthesis) :: 
				getLispToken(trimmed.substring(trimmed.indexOf(LEFT_PARENTHESYS)+LEFT_PARENTHESYS.length())) 
			case Some(rParenthesis) if rParenthesis.toUpperCase().equals(RIGHT_PARENTHESYS) => 
				LispRightParenthesis(rParenthesis) :: 
				getLispToken(trimmed.substring(trimmed.indexOf(RIGHT_PARENTHESYS)+RIGHT_PARENTHESYS.length())) 
			case Some(quote) if quote.toUpperCase().equals(QUOTE) => 
				LispOperator(quote) :: 
				getLispToken(trimmed.substring(trimmed.indexOf(quote)+quote.length()))
			case Some(car) if car.toUpperCase().equals(CAR) => 
				LispOperator(car) :: 
				getLispToken(trimmed.substring(trimmed.indexOf(car)+car.length()))
			case Some(cdr) if cdr.toUpperCase().equals(CDR) => 
				LispOperator(cdr) :: 
				getLispToken(trimmed.substring(trimmed.indexOf(cdr)+cdr.length()))
			case Some(cons) if cons.toUpperCase().equals(CONS) => 
				LispOperator(cons) :: 
				getLispToken(trimmed.substring(trimmed.indexOf(cons)+cons.length()))
			case Some(equal) if equal.toUpperCase().equals(EQUAL) => 
				LispOperator(equal) :: 
				getLispToken(trimmed.substring(trimmed.indexOf(equal)+equal.length()))
			case Some(atom) if atom.toUpperCase().equals(ATOM) => 
				LispOperator(atom) :: 
				getLispToken(trimmed.substring(trimmed.indexOf(atom)+atom.length()))
			case Some(cond) if cond.toUpperCase().equals(COND) => 
				LispOperator(cond) :: 
				getLispToken(trimmed.substring(trimmed.indexOf(cond)+cond.length()))
			case Some(lambda) if lambda.toUpperCase().equals(LAMBDA) => 
				LispOperator(lambda) :: 
				getLispToken(trimmed.substring(trimmed.indexOf(lambda)+lambda.length()))
			case Some(label) if label.toUpperCase().equals(LABEL) => 
				LispOperator(label) :: 
				getLispToken(trimmed.substring(trimmed.indexOf(label)+label.length()))
			case Some(atom) =>
				LispAtom(atom) :: 
				getLispToken(trimmed.substring(trimmed.indexOf(atom)+atom.length()))
			case None => Nil
		}
	}

	var paramToName: HashMap[Integer, AbstractLisp] = null
	var nameToValue: HashMap[AbstractLisp, AbstractLisp] = null

	def doLisp(str: String): String = {

		val strDelimit = ", "
		def transferString(list: List[Any]): String = {
			list match {
				case head :: tails => {
					head match {
						case LispAtom(atom) => {
							( atom + strDelimit + transferString(tails) )
						}
					}
				}
				case Nil => {
					""
				}
			}
		}

		val listLisp = getLispToken(str)
		val (resultLisp, listRemains) = evaluateLisp(listLisp)
		resultLisp match {
			case LispAtom(atom) => atom
			case LispList(list) => {
				val str = transferString(list)
				str.substring(0, str.length - strDelimit.length)
				"(" + str.substring(0, str.length - strDelimit.length) + ")"
			}
			case _ => null
		}

	}

	def evaluateLispOpCAR(param: List[AbstractLisp]): (AbstractLisp, List[AbstractLisp]) = {
		val ( list, remains ) = evaluateLisp(param);
		list match {
			case LispList(hd :: tls) => {
				hd match {
					case LispAtom(atom) => {
						(LispAtom(atom), remains)
					}
					case LispList(list) => {
						(LispList(list), remains)
					}
					case _ => throw new Exception("A: " + hd)
				}
			}
		}
	}

	def evaluateLispOpCDR(param: List[AbstractLisp]): (AbstractLisp, List[AbstractLisp]) = {
		val ( list, remains ) = evaluateLisp(param);
		list match {
			case LispList(hd :: tls) => {
				tls match {
					case List(LispAtom(atom)) => {
						(LispAtom(atom), remains)
					}
					case h :: ts => {
						(LispList(h :: ts), remains)
					}
					case _ => throw new Exception("D: " + tls)
				}
			}
		}
	}

	def evaluateLispOpCONS(param: List[AbstractLisp]): (AbstractLisp, List[AbstractLisp]) = {
		val listAtoms = List()
		
		val ( first, tailsAfterFirst ) = evaluateLisp(param)
		val ( second, tailsAfterSecond ) = evaluateLisp(tailsAfterFirst)

		val ret = first match {
			case LispAtom(f) => {
				second match {
					case LispAtom(s) => {
						LispList(List(first, second))
					}
					case LispList(h :: ts) => {
						LispList(first :: h :: ts)
					}
				}
			}
			case LispList(hd :: tls) => {
				second match {
					case LispAtom(atom) => {
						LispList(hd :: (tls :+ second))
					}
					case LispList(h :: ts) => {
						LispList((hd :: tls) ++ (h :: ts))
					}
				}
			}
		}
		(ret, tailsAfterSecond)
	}

	def evaluateLispOpEQUAL(param: List[AbstractLisp]): (AbstractLisp, List[AbstractLisp]) = {
		val ( first, tailsAfterFirst ) = evaluateLisp(param)
		val ( second, tailsAfterSecond ) = evaluateLisp(tailsAfterFirst)

		(if ( first == second ) LispAtom("T") else LispAtom("Nil"), tailsAfterSecond)
	}

	def evaluateLispOpATOM(param: List[AbstractLisp]): (AbstractLisp, List[AbstractLisp]) = {
		val ( atom, remains ) = evaluateLisp(param)
		
		atom match {
			case LispAtom(a) => (LispAtom("T"), remains)
			case LispList(a) => (LispAtom("T"), remains)
			case _ => throw new Exception("ATOM: " + atom)
		}
	}

	def evaluateLispOpCOND(param: List[AbstractLisp]): (AbstractLisp, List[AbstractLisp]) = {
		var listAtoms = List[AbstractLisp]()
		var bLoop: Boolean = true;

		var headInLoop: AbstractLisp = null
		var tailsInLoop: List[AbstractLisp] = param

		do {
			val ( hInLoop, tsInLoop ) = evaluateLisp(tailsInLoop)
			headInLoop = hInLoop
			tailsInLoop = tsInLoop

			headInLoop match {
				case LispList(list) => {
					listAtoms = listAtoms :+ LispList(list) 
				}
				case null => bLoop = false
				case _ => throw new Exception("COND:headInLoop: " + headInLoop)
			}
		} while( bLoop )

		var result: AbstractLisp = null
9		for( l <- listAtoms) {
			if( null == result ) {
				l match {
					case LispList(first :: second :: Nil) => {
						if( first == LispAtom("T") ) {
							second match {
								case LispAtom(dummy) => result = LispAtom(dummy) 
							}
						}
					}
					case _ => throw new Exception("COND: " + l)
				}
			}
		}
		( result, tailsInLoop )
	}

	def evaluateLispOpLAMBDA(param: List[AbstractLisp]): (AbstractLisp, List[AbstractLisp]) = {

		val (prototype, remains1) = organizeLisp(param)
		val (body, remains2) = organizeLisp(remains1)

		val (prttpListLisp, remains1Nil) = evaluateLisp(prototype)

		val LispList(lst) = prttpListLisp
		val szLispList = lst.size
		var (dummy :: remains) = remains2

		val (first :: second :: Nil) = lst
		val (fValue, tmpRemains1) = organizeLisp(remains)
		val (sValue, tmpRemains2) = organizeLisp(tmpRemains1)

		var lstNew = List[AbstractLisp]()
		body.foreach( i => {
			if( i == first ) {
				lstNew = lstNew ::: fValue
			} else if( i == second ) {
				lstNew = lstNew ::: sValue
			} else {
				lstNew = lstNew :+ i
			}
		})
		evaluateLisp(lstNew)
	}

	def evaluateLispOpLABEL(param: List[AbstractLisp]): (AbstractLisp, List[AbstractLisp]) = {
		val (func, remains1) = evaluateLisp(param)
		val (dummyLP :: atomLambda :: lambda, remains2) = organizeLisp(remains1)
		var (dummyRP :: remains) = remains2
		evaluateLispOpLAMBDA(lambda ::: remains)
	}
	
	def evaluateLispOperator(list: List[AbstractLisp]): (AbstractLisp, List[AbstractLisp]) = {
		list match {
			case LispOperator(operator) :: tails => {

				operator match {
					case "QUOTE" => {
						evaluateLisp(tails)
					}
					case "CAR" => {
						evaluateLispOpCAR(tails)
					}
					case "CDR" => {
						evaluateLispOpCDR(tails)
					}
					case "CONS" => {
						evaluateLispOpCONS(tails)
					}
					case "EQUAL" => {
						evaluateLispOpEQUAL(tails)
					}
					case "ATOM" => {
						evaluateLispOpATOM(tails)
					}
					case "COND" => {
						evaluateLispOpCOND(tails)
					}
					case "LAMBDA" => {
						evaluateLispOpLAMBDA(tails)
					}
					case "LABEL" => {
						evaluateLispOpLABEL(tails)
					}
					case _ => throw new Exception("No Op: " + operator)
				}
			}
			case _ => {
				throw new Exception("Op err: " + list)
			}
		}
	}

	def evaluateLispAtom(list: List[AbstractLisp]): (AbstractLisp, List[AbstractLisp]) = {
		var listAtoms = List[AbstractLisp]()
		var bLoop: Boolean = true;

		var headInLoop: AbstractLisp = null
		var tailsInLoop: List[AbstractLisp] = list

		do {
			val ( hInLoop, tsInLoop ) = evaluateLisp(tailsInLoop);
			headInLoop = hInLoop
			tailsInLoop = tsInLoop

			headInLoop match {
				case LispAtom(atom) => {
					listAtoms = listAtoms :+ headInLoop
				}
				case LispList(list) => {
					listAtoms = listAtoms :+ LispList(list)
				}
				case null => bLoop = false
				case _ => throw new Exception("( middle err")
			}
		} while( bLoop )

		listAtoms match {
			case Nil => {
				(null, tailsInLoop)
			}
			case head :: Nil => {
				(head, tailsInLoop)
			}
			case head :: tails => {
				(LispList(listAtoms), tailsInLoop)
			}
			case _ => {
				throw new Exception("( err")
			}
		}
	}

	def evaluateLisp(list: List[AbstractLisp]): (AbstractLisp, List[AbstractLisp]) = {
		list match {
			case LispLeftParenthesis("(") :: tails => {
				val ( h :: ts ) = tails
				h match {
					case LispOperator(op) => {
						val (atom, remains) = evaluateLispOperator(tails)
						val (rParenthesis, remainsNil) = evaluateLisp(remains)
						rParenthesis match {
							case null => (atom, remainsNil)
							case _ => throw new Exception("(:" + h)
						}
					}
					case _ => {
						val (x, y) = evaluateLispAtom(tails)
						(x, y)
					}
				}
			}
			case LispAtom(atom) :: tails => {
				( LispAtom(atom), tails )
			} 
			case LispRightParenthesis(")") :: tails => {
				(null, tails)
			}
			case Nil => {
				(null, Nil)
			}
			case _ => throw new Exception("Syntax Error: " + list)
		}
	}
	
	def organizeLisp(list: List[AbstractLisp]): (List[AbstractLisp], List[AbstractLisp]) = {
		var listResult = List[AbstractLisp]()

		var tails: List[AbstractLisp] = list

		if( tails.head == LispLeftParenthesis("(") ) {
			var bLoop: Boolean = true;
			var bDepth = 0
			do {
				val (head :: ts) = tails
				tails = ts
	
				if (head == LispLeftParenthesis("(")) bDepth = bDepth + 1 
				else if(head == LispRightParenthesis(")")) bDepth = bDepth - 1
	
				listResult = listResult :+ head
	
				if (bDepth == 0) bLoop = false
				
			} while (bLoop)
		}

		(listResult, tails)
	}
	
}
