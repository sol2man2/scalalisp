package com.lge.swcdc.scalalisp.test

import com.lge.swcdc.scalalisp._
import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

class LispRule10FunSuite extends FunSuite with BeforeAndAfter {

	var lisp: Lisp = _

	before {
		lisp = new Lisp
	}

	val pretest1Rule10 = "(ATOM (QUOTE (A B)))"
	val resultPretest1Rule10 = List( LispLeftParenthesis("("), LispOperator("ATOM"), LispLeftParenthesis("("),   
				LispOperator("QUOTE"), LispLeftParenthesis("("), LispAtom("A"), LispAtom("B"), LispRightParenthesis(")"), 
				LispRightParenthesis(")"), LispRightParenthesis(")")
			)
	val firstPretest1Rule10 = resultPretest1Rule10 
	val secondPretest1Rule10 = Nil

	ignore("pretest #1: Rule 10") {
		val lispToken = lisp.getLispToken(pretest1Rule10)
		assert( lispToken === resultPretest1Rule10 )

		val (first, second) = lisp.organizeLisp(lispToken)
		assert( first === firstPretest1Rule10 )
		assert( second === secondPretest1Rule10 )
	}

//	val rule10 = "((LABEL f (LAMBDA (v1 ... vn) e)) e1 I ... en)"
//				=> ((LAMBDA (v1 ... vn) e) e1 ... en)
	val rule10 = "((LABEL func (LAMBDA (A B) (QUOTE (A B)))) (QUOTE X) (QUOTE Y))"
	val resultRule10 = LispList(List(LispAtom("X"), LispAtom("Y")))
	val finalResultRule10 = "(X, Y)"

	test("test: Rule 10") {
		val lispToken = lisp.getLispToken(rule10)
		assert( lispToken === 
			List( LispLeftParenthesis("("), LispLeftParenthesis("("), LispOperator("LABEL"), LispAtom("func"),
				LispLeftParenthesis("("), LispOperator("LAMBDA"), 
					LispLeftParenthesis("("), LispAtom("A"), LispAtom("B"), LispRightParenthesis(")"),
						LispLeftParenthesis("("), LispOperator("QUOTE"), 
							LispLeftParenthesis("("), LispAtom("A"), LispAtom("B"), LispRightParenthesis(")"),
						LispRightParenthesis(")"),
					LispRightParenthesis(")"), 
				LispRightParenthesis(")"),
				LispLeftParenthesis("("), LispOperator("QUOTE"), LispAtom("X"), LispRightParenthesis(")"),
				LispLeftParenthesis("("), LispOperator("QUOTE"), LispAtom("Y"), LispRightParenthesis(")"),
			LispRightParenthesis(")")
			) 
		)

		val (resultLisp, listRemains) = lisp.evaluateLisp(lispToken)
		assert( resultLisp === resultRule10 )

//		val resultDone = lisp.doLisp(rule10)
//		assert( resultDone === finalResultRule10 )
	}

	after {
		
	}

}
