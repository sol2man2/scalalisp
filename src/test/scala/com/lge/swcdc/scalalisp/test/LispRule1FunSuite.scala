
package com.lge.swcdc.scalalisp.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite
import com.lge.swcdc.scalalisp._

class LispRule1FunSuite extends FunSuite with BeforeAndAfter {
	
	var lisp: Lisp = _

	before {
		lisp = new Lisp
	}

	ignore("This is Scala's Lisp") {
		assert( 2 === 2 )
	}

	test("test: RegEx pattern match") {
		var str = "("
		var resultMatch = lisp.regexLeftParenthesis.findFirstMatchIn(str)
		assert(resultMatch.isEmpty === false)
		assert(resultMatch.get.matched === "(")

		str = ")"
		resultMatch = lisp.regexLeftParenthesis.findFirstMatchIn(str)
		assert(resultMatch.isEmpty === false)
		assert(resultMatch.get.matched === ")")

		str = "quote"
		resultMatch = lisp.regexLeftParenthesis.findFirstMatchIn(str)
		assert(resultMatch.isEmpty === false)
		assert(resultMatch.get.matched === "quote")

		str = "car "
		resultMatch = lisp.regexLeftParenthesis.findFirstMatchIn(str)
		assert(resultMatch.isEmpty === false)
		assert(resultMatch.get.matched === "car")

		str = "CDR"
		resultMatch = lisp.regexLeftParenthesis.findFirstMatchIn(str)
		assert(resultMatch.isEmpty === false)
		assert(resultMatch.get.matched === "CDR")
	}

	test("test: getToken()") {
		assert( lisp.getLispToken(" ( ") === List(LispLeftParenthesis(lisp.LEFT_PARENTHESYS)) )
		assert( lisp.getLispToken(" ) ") === List(LispRightParenthesis(lisp.RIGHT_PARENTHESYS)) )

		assert( lisp.getLispToken("Quote a b c") === 
			List(LispOperator("Quote"), LispAtom("a"), LispAtom("b"), LispAtom("c")) )
		assert( lisp.getLispToken("(QUOTE (X Y Z))") === 
			List(LispLeftParenthesis(lisp.LEFT_PARENTHESYS), LispOperator("QUOTE"), 
				LispLeftParenthesis(lisp.LEFT_PARENTHESYS), LispAtom("X"), LispAtom("Y"), LispAtom("Z"),
				LispRightParenthesis(lisp.RIGHT_PARENTHESYS), LispRightParenthesis(lisp.RIGHT_PARENTHESYS)) )
			
		assert( lisp.getLispToken("CAR abs") === 
			List(LispOperator("CAR"), LispAtom("abs")) )
		assert( lisp.getLispToken(" quotE a cAr b cdR c") === 
			List(LispOperator("quotE"), LispAtom("a"), LispOperator("cAr"), 
				LispAtom("b"), LispOperator("cdR"), LispAtom("c")) )

		assert( lisp.getLispToken(" (QUOTE a car )") === 
			List(LispLeftParenthesis(lisp.LEFT_PARENTHESYS), LispOperator("QUOTE"), 
				LispAtom("a"), LispOperator("car"), LispRightParenthesis(lisp.RIGHT_PARENTHESYS)) )
		assert( lisp.getLispToken("(QUOTE A )") === 
			List(LispLeftParenthesis(lisp.LEFT_PARENTHESYS), LispOperator("QUOTE"), LispAtom("A"), 
				LispRightParenthesis(lisp.RIGHT_PARENTHESYS)) )
	}

	test("test: evaluateLisp()") {
		var lispToken = List(LispLeftParenthesis("("), LispAtom("test"), 
			LispAtom("is"), LispAtom("huk"), LispRightParenthesis(")"))
		val (resultLisp, listRemains) = lisp.evaluateLisp(lispToken)

		assert( listRemains === Nil)
		assert( resultLisp === LispList( List(LispAtom("test"), LispAtom("is"), LispAtom("huk")) ) )

		lispToken = List(LispLeftParenthesis("("), LispAtom("test"), 
			LispAtom("is"), LispAtom("huk"), LispRightParenthesis(")"));
		val (resultLisp1, listRemains1) = lisp.evaluateLisp(lispToken);
		assert( resultLisp1 === LispList( List(LispAtom("test"), LispAtom("is"), LispAtom("huk")) ) )
	}

	val pretest1Rule1 = "( )"
	val resultPretest1Rule1 = null

	test("pretest #1: Rule 1") {
		val lispToken = lisp.getLispToken(pretest1Rule1)
		val lispList = List(LispLeftParenthesis("("), LispRightParenthesis(")"))
		
		assert( lispToken === lispList )
				
		val (resultLisp, listRemains) = lisp.evaluateLisp(lispToken)
		assert( resultLisp === resultPretest1Rule1 )
	}

	val pretest2Rule1 = "(A)"
	val resultPretest2Rule1 = LispAtom("A")

	test("pretest #2: Rule 1") {
		val lispToken = lisp.getLispToken(pretest2Rule1)
		val lispList = List(LispLeftParenthesis("("), LispAtom("A"), LispRightParenthesis(")"))
		
		assert( lispToken === lispList )
				
		val (resultLisp, listRemains) = lisp.evaluateLisp(lispToken)
		assert( resultLisp === resultPretest2Rule1 )
	}

	val pretest3Rule1 = "(O P Q R)"
	val resultPretest3Rule1 = LispList(List(LispAtom("O"), LispAtom("P"), LispAtom("Q"), LispAtom("R")))

	test("pretest #3: Rule 1") {
		val lispToken = lisp.getLispToken(pretest3Rule1)
		val lispList = List(LispLeftParenthesis("("), LispAtom("O"), 
				LispAtom("P"), LispAtom("Q"), LispAtom("R"), LispRightParenthesis(")"))
		
		assert( lispToken === lispList )
				
		val (resultLisp, listRemains) = lisp.evaluateLisp(lispToken)
		assert( listRemains === Nil)
		assert( resultLisp === resultPretest3Rule1 )
	}

	val pretest4Rule1 = "(QUOTE (A B C))"
	val resultPretest4Rule1 = LispList(List(LispAtom("A"), LispAtom("B"), LispAtom("C")))

	test("pretest #5: Rule 1") {
		val lispToken = lisp.getLispToken(pretest4Rule1)
		val lispList = List(LispLeftParenthesis("("), LispOperator("QUOTE"), 
				LispLeftParenthesis("("), LispAtom("A"), LispAtom("B"), LispAtom("C"), LispRightParenthesis(")"), 
				LispRightParenthesis(")"))

		assert( lispToken === lispList )

		val (resultLisp, listRemains) = lisp.evaluateLisp(lispToken)
		assert( listRemains === Nil)
		assert( resultLisp === resultPretest4Rule1 )
	}

	val rule1 = "(QUOTE A)"
	val resultRule1 = LispAtom("A")
	val finalResultRule1 = "A"

	test("test #1: Rule 1") {
		val lispToken = lisp.getLispToken(rule1)
		assert( lispToken === 
			List(LispLeftParenthesis("("), LispOperator("QUOTE"), LispAtom("A"), LispRightParenthesis(")")) )

		val (resultLisp, listRemains) = lisp.evaluateLisp(lispToken)
		assert( listRemains === Nil)
		assert( resultLisp === resultRule1 )

		val resultDone = lisp.doLisp(rule1)
		assert( resultDone === finalResultRule1 )
	}

	after {
		
	}

}
