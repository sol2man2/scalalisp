
package com.lge.swcdc.scalalisp.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite
import com.lge.swcdc.scalalisp._

class LispRule2FunSuite extends FunSuite with BeforeAndAfter {
	
	var lisp: Lisp = _

	before {
		lisp = new Lisp
	}

	val rule2 = "(CAR (QUOTE (A B C)))"
	val resultRule2 = LispAtom("A")
	val finalResultRule2 = "A"

	test("test: Rule 2") {
		val lispToken = lisp.getLispToken(rule2)
		assert( lispToken === 
			List( LispLeftParenthesis("("), LispOperator("CAR"), LispLeftParenthesis("("),
				LispOperator("QUOTE"), LispLeftParenthesis("("),
				LispAtom("A"), LispAtom("B"), LispAtom("C"), LispRightParenthesis(")"), 
				LispRightParenthesis(")"), LispRightParenthesis(")"))
			)

		val (resultLisp, listRemains) = lisp.evaluateLisp(lispToken)
		assert( listRemains === Nil )
		assert( resultLisp === resultRule2 )

		val resultDone = lisp.doLisp(rule2)
		assert( resultDone === finalResultRule2 )
	}

	after {
		
	}

}
