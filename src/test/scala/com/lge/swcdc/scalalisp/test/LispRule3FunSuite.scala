
package com.lge.swcdc.scalalisp.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite
import com.lge.swcdc.scalalisp._

class LispRule3FunSuite extends FunSuite with BeforeAndAfter {

	var lisp: Lisp = _

	before {
		lisp = new Lisp
	}

	val rule3 = "(CDR (QUOTE (A B C)))"
	val resultRule3 = LispList(List(LispAtom("B"), LispAtom("C")))
	val finalResultRule3 = "(B, C)"

	test("test: Rule 3") {
		val lispToken = lisp.getLispToken(rule3)
		assert( lispToken === 
			List( LispLeftParenthesis("("), LispOperator("CDR"), LispLeftParenthesis("("),
				LispOperator("QUOTE"), LispLeftParenthesis("("),
				LispAtom("A"), LispAtom("B"), LispAtom("C"), LispRightParenthesis(")"), 
				LispRightParenthesis(")"), LispRightParenthesis(")")) 
			)

		val (resultLisp, listRemains) = lisp.evaluateLisp(lispToken)
		assert( listRemains === Nil )
		assert( resultLisp === resultRule3 )

		val resultDone = lisp.doLisp(rule3)
		assert( resultDone === finalResultRule3 )
	}

	after {
		
	}

}
