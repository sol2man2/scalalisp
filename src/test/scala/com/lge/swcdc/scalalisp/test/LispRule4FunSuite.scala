
package com.lge.swcdc.scalalisp.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite
import com.lge.swcdc.scalalisp._

class LispRule4FunSuite extends FunSuite with BeforeAndAfter {
	
	var lisp: Lisp = _

	before {
		lisp = new Lisp
	}

	val pretest1Rule4 = "(CONS (QUOTE (X)) (QUOTE Y))"
	val resultPretest1Rule4 = LispList(List(LispAtom("X"), LispAtom("Y")))
	val finalResultPretest1Rule4 = "(X, Y)"

	test("pretest #1: Rule 4") {
		val lispToken = lisp.getLispToken(pretest1Rule4)
		assert( lispToken === 
			List( LispLeftParenthesis("("), LispOperator("CONS"), 
				LispLeftParenthesis("("), LispOperator("QUOTE"), 
					LispLeftParenthesis("("), LispAtom("X"), LispRightParenthesis(")"), 
				LispRightParenthesis(")"), 
				LispLeftParenthesis("("), LispOperator("QUOTE"), LispAtom("Y"), LispRightParenthesis(")"), 
				LispRightParenthesis(")")) 
			)

		val (resultLisp, listRemains) = lisp.evaluateLisp(lispToken)
		assert( resultLisp === resultPretest1Rule4 )

		val resultDone = lisp.doLisp(pretest1Rule4)
		assert( resultDone === finalResultPretest1Rule4 )
	}

	val pretest2Rule4 = "(CONS (QUOTE (L M)) (QUOTE N))"
	val resultPretest2Rule4 = LispList(List(LispAtom("L"), LispAtom("M"), LispAtom("N")))
	val finalResultPretest2Rule4 = "(L, M, N)"

	test("pretest #2: Rule 4") {
		val lispToken = lisp.getLispToken(pretest2Rule4)
		assert( lispToken === 
			List( LispLeftParenthesis("("), LispOperator("CONS"), 
				LispLeftParenthesis("("), LispOperator("QUOTE"), 
					LispLeftParenthesis("("), LispAtom("L"), LispAtom("M"), LispRightParenthesis(")"),
				LispRightParenthesis(")"), 
				LispLeftParenthesis("("), LispOperator("QUOTE"), LispAtom("N"), LispRightParenthesis(")"), 
				LispRightParenthesis(")")) 
			)

		val (resultLisp, listRemains) = lisp.evaluateLisp(lispToken)
		assert( listRemains === Nil)
		assert( resultLisp === resultPretest2Rule4 )

		val resultDone = lisp.doLisp(pretest2Rule4)
		assert( resultDone === finalResultPretest2Rule4 )
	}

	val pretest3Rule4 = "(CONS (QUOTE (O P)) (QUOTE (Q R)))"
	val resultPretest3Rule4 = LispList(List(LispAtom("O"), LispAtom("P"), LispAtom("Q"), LispAtom("R")))
	val finalResultPretest3Rule4 = "(O, P, Q, R)"

	test("pretest #3: Rule 4") {
		val lispToken = lisp.getLispToken(pretest3Rule4)
		assert( lispToken === 
			List( LispLeftParenthesis("("), LispOperator("CONS"), 
				LispLeftParenthesis("("), LispOperator("QUOTE"), 
					LispLeftParenthesis("("), LispAtom("O"), LispAtom("P"), LispRightParenthesis(")"),
				LispRightParenthesis(")"), 
				LispLeftParenthesis("("), LispOperator("QUOTE"), 
					LispLeftParenthesis("("), LispAtom("Q"), LispAtom("R"), LispRightParenthesis(")"),
				LispRightParenthesis(")"), 
				LispRightParenthesis(")")) 
			)

		val (resultLisp, listRemains) = lisp.evaluateLisp(lispToken)
		assert( resultLisp === resultPretest3Rule4 )

		val resultDone = lisp.doLisp(pretest3Rule4)
		assert( resultDone === finalResultPretest3Rule4 )
	}

	val rule4 = "(CONS (QUOTE A) (QUOTE (B C)))"
	val resultRule4 = LispList(List(LispAtom("A"), LispAtom("B"), LispAtom("C")))
	val finalResultRule4 = "(A, B, C)"

	test("test: Rule 4") {
		val lispToken = lisp.getLispToken(rule4)
		assert( lispToken === 
			List( LispLeftParenthesis("("), LispOperator("CONS"), 
				LispLeftParenthesis("("), LispOperator("QUOTE"), LispAtom("A"), LispRightParenthesis(")"), 
				LispLeftParenthesis("("), LispOperator("QUOTE"), 
				LispLeftParenthesis("("), LispAtom("B"), LispAtom("C"), LispRightParenthesis(")"), 
				LispRightParenthesis(")"), LispRightParenthesis(")")) 
			)

		val (resultLisp, listRemains) = lisp.evaluateLisp(lispToken)
		assert( resultLisp === resultRule4 )

		val resultDone = lisp.doLisp(rule4)
		assert( resultDone === finalResultRule4 )
	}

	after {
		
	}

}
