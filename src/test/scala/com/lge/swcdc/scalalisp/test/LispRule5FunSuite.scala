package com.lge.swcdc.scalalisp.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite
import com.lge.swcdc.scalalisp._

class LispRule5FunSuite extends FunSuite with BeforeAndAfter {
	
	var lisp: Lisp = _

	before {
		lisp = new Lisp
	}

	val pretest1Rule5 = "(QUOTE (A B))"
	val resultPretest1Rule5 = LispList(List(LispAtom("A"), LispAtom("B")))
	val finalResultPretest1Rule5 = "(A, B)"

	test("pretest #1: Rule 5") {
		val lispToken = lisp.getLispToken(pretest1Rule5)
		assert( lispToken === 
			List( LispLeftParenthesis("("), LispOperator("QUOTE"),  
				LispLeftParenthesis("("), LispAtom("A"), LispAtom("B"), LispRightParenthesis(")"), 
				LispRightParenthesis(")") 
			) 
		)

		val (resultLisp, listRemains) = lisp.evaluateLisp(lispToken)
		assert( resultLisp === resultPretest1Rule5 )

		val resultDone = lisp.doLisp(pretest1Rule5)
		assert( resultDone === finalResultPretest1Rule5 )
	}

	val pretest2Rule5 = "(CAR (QUOTE (D E)))"
	val resultPretest2Rule5 = LispAtom("D")
	val finalResultPretest2Rule5 = "D"

	test("pretest #2: Rule 5") {
		val lispToken = lisp.getLispToken(pretest2Rule5)
		assert( lispToken === 
			List( LispLeftParenthesis("("), LispOperator("CAR"),
				LispLeftParenthesis("("), LispOperator("QUOTE"),  
				LispLeftParenthesis("("), LispAtom("D"), LispAtom("E"), LispRightParenthesis(")"), 
				LispRightParenthesis(")"), 
				LispRightParenthesis(")") 
			) 
		)

		val (resultLisp, listRemains) = lisp.evaluateLisp(lispToken)
		assert( resultLisp === resultPretest2Rule5 )

		val resultDone = lisp.doLisp(pretest2Rule5)
		assert( resultDone === finalResultPretest2Rule5 )
	}

	val pretest3Rule5 = "(QUOTE ((CAR (QUOTE (D E))) (QUOTE F)))"
	val resultPretest3Rule5 = LispList(List(LispAtom("D"), LispAtom("F")))
	val finalResultPretest3Rule5 = "(D, F)"

	test("pretest #3: Rule 5") {
		val lispToken = lisp.getLispToken(pretest3Rule5)
		assert( lispToken === 
			List( LispLeftParenthesis("("), LispOperator("QUOTE"), LispLeftParenthesis("("), 
				LispLeftParenthesis("("), LispOperator("CAR"),
				LispLeftParenthesis("("), LispOperator("QUOTE"),  
				LispLeftParenthesis("("), LispAtom("D"), LispAtom("E"),
				LispRightParenthesis(")"), 
				LispRightParenthesis(")"),
				LispRightParenthesis(")"),
				LispLeftParenthesis("("), LispOperator("QUOTE"), LispAtom("F"), 
				LispRightParenthesis(")"), LispRightParenthesis(")"),
				LispRightParenthesis(")") 
			) 
		)

		val (resultLisp, listRemains) = lisp.evaluateLisp(lispToken)
		assert( resultLisp === resultPretest3Rule5 )

		val resultDone = lisp.doLisp(pretest3Rule5)
		assert( resultDone === finalResultPretest3Rule5 )
	}

	val rule5 = "(EQUAL (CAR (QUOTE (A B))) (QUOTE A))"
	val resultRule5 = LispAtom("T")
	val finalResultRule5 = "T"

	test("test: Rule 5") {
		val lispToken = lisp.getLispToken(rule5)
		assert( lispToken === 
			List( LispLeftParenthesis("("), LispOperator("EQUAL"),
				LispLeftParenthesis("("), LispOperator("CAR"),
					LispLeftParenthesis("("), LispOperator("QUOTE"),
					LispLeftParenthesis("("), LispAtom("A"), LispAtom("B"), LispRightParenthesis(")"), 
					LispRightParenthesis(")"),
				LispRightParenthesis(")"),
				LispLeftParenthesis("("), LispOperator("QUOTE"), LispAtom("A"), LispRightParenthesis(")"), 
				LispRightParenthesis(")")
			) 
		)

		val (resultLisp, listRemains) = lisp.evaluateLisp(lispToken)
		assert( resultLisp === resultRule5 )

		val resultDone = lisp.doLisp(rule5)
		assert( resultDone === finalResultRule5 )
	}

	after {
		
	}

}
