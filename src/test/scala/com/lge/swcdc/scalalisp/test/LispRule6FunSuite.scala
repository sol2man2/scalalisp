package com.lge.swcdc.scalalisp.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite
import com.lge.swcdc.scalalisp._

class LispRule6FunSuite extends FunSuite with BeforeAndAfter {

	var lisp: Lisp = _

	before {
		lisp = new Lisp
	}

	val pretest1Rule6 = "(ATOM (QUOTE (A B)))"
	val resultPretest1Rule6 = LispAtom("T")
	val finalResultPretest1Rule6 = "T"

	test("pretest #1: Rule 6") {
		val lispToken = lisp.getLispToken(pretest1Rule6)
		assert( lispToken === 
			List( LispLeftParenthesis("("), LispOperator("ATOM"), LispLeftParenthesis("("),   
				LispOperator("QUOTE"), LispLeftParenthesis("("), LispAtom("A"), LispAtom("B"), LispRightParenthesis(")"), 
				LispRightParenthesis(")"), LispRightParenthesis(")")
			) 
		)

		val (resultLisp, listRemains) = lisp.evaluateLisp(lispToken)
		assert( resultLisp === resultPretest1Rule6 )

		val resultDone = lisp.doLisp(pretest1Rule6)
		assert( resultDone === finalResultPretest1Rule6 )
	}

	val rule6 = "(ATOM e)"
	val resultRule6 = LispAtom("T")
	val finalResultRule6 = "T"

	test("test: Rule 6") {
		val lispToken = lisp.getLispToken(rule6)
		assert( lispToken === 
			List( LispLeftParenthesis("("), LispOperator("ATOM"), LispAtom("e"), LispRightParenthesis(")")
			) 
		)

		val (resultLisp, listRemains) = lisp.evaluateLisp(lispToken)
		assert( resultLisp === resultRule6 )

		val resultDone = lisp.doLisp(rule6)
		assert( resultDone === finalResultRule6 )
	}

	after {
		
	}

}