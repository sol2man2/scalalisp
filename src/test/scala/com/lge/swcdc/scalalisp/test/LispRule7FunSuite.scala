package com.lge.swcdc.scalalisp.test

import com.lge.swcdc.scalalisp._
import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

class LispRule7FunSuite extends FunSuite with BeforeAndAfter {
	
	var lisp: Lisp = _

	before {
		lisp = new Lisp
	}

	val pretest1Rule7 = "(ATOM (QUOTE (A B)))"
	val resultPretest1Rule7 = LispAtom("T")
	val finalResultPretest1Rule7 = "T"

	test("pretest #1: Rule 7") {
		val lispToken = lisp.getLispToken(pretest1Rule7)
		assert( lispToken === 
			List( LispLeftParenthesis("("), LispOperator("ATOM"), LispLeftParenthesis("("),   
				LispOperator("QUOTE"), LispLeftParenthesis("("), LispAtom("A"), LispAtom("B"), LispRightParenthesis(")"), 
				LispRightParenthesis(")"), LispRightParenthesis(")")
			) 
		)

		val (resultLisp, listRemains) = lisp.evaluateLisp(lispToken)
		assert( resultLisp === resultPretest1Rule7 )

		val resultDone = lisp.doLisp(pretest1Rule7)
		assert( resultDone === finalResultPretest1Rule7 )
	}

	val rule7 = "(COND ((ATOM (QUOTE A)) (QUOTE B)) ((QUOTE T) (QUOTE C)))"
	val resultRule7 = LispAtom("B")
	val finalResultRule7 = "B"

	test("test: Rule 7") {
		val lispToken = lisp.getLispToken(rule7)
		assert( lispToken === 
			List( LispLeftParenthesis("("), LispOperator("COND"), 
				LispLeftParenthesis("("), LispLeftParenthesis("("), 
					LispOperator("ATOM"), 
						LispLeftParenthesis("("), LispOperator("QUOTE"), LispAtom("A"), LispRightParenthesis(")"),
					LispRightParenthesis(")"),
					LispLeftParenthesis("("), LispOperator("QUOTE"), LispAtom("B"), LispRightParenthesis(")"),
				LispRightParenthesis(")"),
				LispLeftParenthesis("("), 
					LispLeftParenthesis("("), LispOperator("QUOTE"), LispAtom("T"), LispRightParenthesis(")"),
					LispLeftParenthesis("("), LispOperator("QUOTE"), LispAtom("C"), LispRightParenthesis(")"),
				LispRightParenthesis(")"), LispRightParenthesis(")")
			) 
		)

		val (resultLisp, listRemains) = lisp.evaluateLisp(lispToken)
		assert( resultLisp === resultRule7 )

		val resultDone = lisp.doLisp(rule7)
		assert( resultDone === finalResultRule7 )
	}

	after {
		
	}

}