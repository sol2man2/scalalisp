package com.lge.swcdc.scalalisp.test

import com.lge.swcdc.scalalisp._
import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

class LispRule8FunSuite extends FunSuite with BeforeAndAfter {
	
	var lisp: Lisp = _

	before {
		lisp = new Lisp
	}

	val pretest1Rule8 = "(ATOM (QUOTE (A B)))"
	val resultPretest1Rule8 = LispAtom("T")
	val finalResultPretest1Rule8 = "T"

	ignore("pretest #1: Rule 8") {
		val lispToken = lisp.getLispToken(pretest1Rule8)
		assert( lispToken === 
			List( LispLeftParenthesis("("), LispOperator("ATOM"), LispLeftParenthesis("("),   
				LispOperator("QUOTE"), LispLeftParenthesis("("), LispAtom("A"), LispAtom("B"), LispRightParenthesis(")"), 
				LispRightParenthesis(")"), LispRightParenthesis(")")
			) 
		)

		val (resultLisp, listRemains) = lisp.evaluateLisp(lispToken)
		assert( resultLisp === resultPretest1Rule8 )

		val resultDone = lisp.doLisp(pretest1Rule8)
		assert( resultDone === finalResultPretest1Rule8 )
	}

	val rule8 = "(COND ((ATOM (QUOTE A)) (QUOTE B)) ((QUOTE T) (QUOTE C)))"
	val resultRule8 = LispAtom("B")
	val finalResultRule8 = "B"

	ignore("test: Rule 8") {
		val lispToken = lisp.getLispToken(rule8)
		assert( lispToken === 
			List( LispLeftParenthesis("("), LispOperator("COND"), 
				LispLeftParenthesis("("), LispLeftParenthesis("("), 
					LispOperator("ATOM"), 
						LispLeftParenthesis("("), LispOperator("QUOTE"), LispAtom("A"), LispRightParenthesis(")"),
					LispRightParenthesis(")"),
					LispLeftParenthesis("("), LispOperator("QUOTE"), LispAtom("B"), LispRightParenthesis(")"),
				LispRightParenthesis(")"),
				LispLeftParenthesis("("), 
					LispLeftParenthesis("("), LispOperator("QUOTE"), LispAtom("T"), LispRightParenthesis(")"),
					LispLeftParenthesis("("), LispOperator("QUOTE"), LispAtom("C"), LispRightParenthesis(")"),
				LispRightParenthesis(")"), LispRightParenthesis(")")
			) 
		)

		val (resultLisp, listRemains) = lisp.evaluateLisp(lispToken)
		assert( resultLisp === resultRule8 )

		val resultDone = lisp.doLisp(rule8)
		assert( resultDone === finalResultRule8 )
	}

	after {
		
	}

}