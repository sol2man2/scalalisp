package com.lge.swcdc.scalalisp.test

import com.lge.swcdc.scalalisp._
import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

class LispRule9FunSuite extends FunSuite with BeforeAndAfter {

	var lisp: Lisp = _

	before {
		lisp = new Lisp
	}

	val pretest1Rule9 = "(ATOM (QUOTE (A B)))"
	val resultPretest1Rule9 = List( LispLeftParenthesis("("), LispOperator("ATOM"), LispLeftParenthesis("("),   
				LispOperator("QUOTE"), LispLeftParenthesis("("), LispAtom("A"), LispAtom("B"), LispRightParenthesis(")"), 
				LispRightParenthesis(")"), LispRightParenthesis(")")
			)
	val firstPretest1Rule9 = resultPretest1Rule9 
	val secondPretest1Rule9 = Nil

	test("pretest #1: Rule 9") {
		val lispToken = lisp.getLispToken(pretest1Rule9)
		assert( lispToken === resultPretest1Rule9 )

		val (first, second) = lisp.organizeLisp(lispToken)
		assert( first === firstPretest1Rule9 )
		assert( second === secondPretest1Rule9 )
	}

	val pretest2Rule9 = " (QUOTE (CONS (A B))) CAR ()"
	val resultPretest2Rule9 = List( LispLeftParenthesis("("), LispOperator("QUOTE"), LispLeftParenthesis("("),   
				LispOperator("CONS"), LispLeftParenthesis("("), LispAtom("A"), LispAtom("B"), LispRightParenthesis(")"), 
				LispRightParenthesis(")"), LispRightParenthesis(")"),
				LispOperator("CAR"), LispLeftParenthesis("("), LispRightParenthesis(")")
			)
	val firstPretest2Rule9 = List( LispLeftParenthesis("("), LispOperator("QUOTE"), LispLeftParenthesis("("),   
				LispOperator("CONS"), LispLeftParenthesis("("), LispAtom("A"), LispAtom("B"), LispRightParenthesis(")"), 
				LispRightParenthesis(")"), LispRightParenthesis(")")
			)
	val secondPretest2Rule9 = List( 
				LispOperator("CAR"), LispLeftParenthesis("("), LispRightParenthesis(")")
			)

	test("pretest #2: Rule 9") {
		val lispToken = lisp.getLispToken(pretest2Rule9)
		assert( lispToken === resultPretest2Rule9 )

		val (first, second) = lisp.organizeLisp(lispToken)
		assert( first === firstPretest2Rule9 )
		assert( second === secondPretest2Rule9 )
	}

	val rule9 = "((LAMBDA (X Y) (CONS (CAR X) Y)) (QUOTE (A B)) (CDR (QUOTE (C D))))"
	val resultRule9 = LispList(List(LispAtom("A"), LispAtom("D")))
	val finalResultRule9 = "(A, D)"

	test("test: Rule 9") {
		val lispToken = lisp.getLispToken(rule9)
		assert( lispToken === 
			List( LispLeftParenthesis("("), LispLeftParenthesis("("), LispOperator("LAMBDA"), 
				LispLeftParenthesis("("), LispAtom("X"), LispAtom("Y"), LispRightParenthesis(")"),
					LispLeftParenthesis("("), LispOperator("CONS"), 
						LispLeftParenthesis("("), LispOperator("CAR"), LispAtom("X"), LispRightParenthesis(")"),
						LispAtom("Y"), LispRightParenthesis(")"),
					LispRightParenthesis(")"),
					LispLeftParenthesis("("), LispOperator("QUOTE"), 
						LispLeftParenthesis("("), LispAtom("A"), LispAtom("B"), LispRightParenthesis(")"),
				LispRightParenthesis(")"),
				LispLeftParenthesis("("), LispOperator("CDR"), 
					LispLeftParenthesis("("), LispOperator("QUOTE"), 
						LispLeftParenthesis("("), LispAtom("C"), LispAtom("D"), LispRightParenthesis(")"),
				LispRightParenthesis(")"), LispRightParenthesis(")"), LispRightParenthesis(")")
			) 
		)

		val (resultLisp, listRemains) = lisp.evaluateLisp(lispToken)
		assert( listRemains === Nil )
		assert( resultLisp === resultRule9 )

		val resultDone = lisp.doLisp(rule9)
		assert( resultDone === finalResultRule9 )
	}

	after {
		
	}

}